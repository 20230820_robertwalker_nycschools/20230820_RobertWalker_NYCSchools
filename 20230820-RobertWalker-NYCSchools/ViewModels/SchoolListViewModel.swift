//
//  SchoolListViewModel.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import Foundation

class SchoolListViewModel: ObservableObject {

    enum ViewModelState {
        case loaded([SchoolViewModel])
        case loading
        case error
    }

    @Published var state: ViewModelState = .loading

    private let api: JsonApi

    init(api: JsonApi = JsonApi()) {
        self.api = api
    }

    @MainActor func refresh() {
        self.fetchData()
    }

    @MainActor private func fetchData() {
        //if refreshing from error state, show loading state
        if case ViewModelState.error = state {
            setState(.loading)
        }

        Task.init(priority: .high) {
            do {
                //fetch school info and scores in parallel
                async let schoolsInfoResponse = fetchSchools()
                async let schoolsScoreResponse = fetchSATScores()

                let (schoolInfos, schoolScores) = try await (schoolsInfoResponse, schoolsScoreResponse)

                let viewModels = transform(schoolInfos: schoolInfos, scores: schoolScores)

                setState(.loaded(viewModels))
            } catch {
                setState(.error)
            }
        }
    }

    //updates view model state on main thread
    @MainActor private func setState(_ state: ViewModelState) {
        self.state = state
    }

    //creates view models by combining school info and scores
    private func transform(schoolInfos: [SchoolInfo], scores: [SchoolScores]) -> [SchoolViewModel] {
        //create lookup map
        let scoreDbnMap: [String: SchoolScores] = scores.reduce(into: [:]) { result, scores in
            result[scores.dbn] = scores
        }

        //create view models
        return schoolInfos.map { info in
            let scores = scoreDbnMap[info.dbn]
            return SchoolViewModel(schoolInfo: info, schoolScores: scores)
        }
    }

    private func fetchSchools() async throws -> [SchoolInfo] {
        return try await api.performRequest(urlString: Constants.schoolUrlString)
    }

    private func fetchSATScores() async throws -> [SchoolScores] {
        return try await api.performRequest(urlString: Constants.satUrlString)
    }
}

extension SchoolListViewModel {
    private struct Constants {
        static let schoolUrlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let satUrlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
}

