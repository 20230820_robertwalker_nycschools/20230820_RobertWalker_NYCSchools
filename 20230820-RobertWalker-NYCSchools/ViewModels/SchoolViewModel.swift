//
//  SchoolViewModel.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import Foundation

struct SchoolViewModel {
    let schoolInfo: SchoolInfo
    let schoolScores: SchoolScores?

    var hasScores: Bool {
        return schoolScores != nil
    }
}

extension SchoolViewModel: Identifiable {
    var id: String {
        return schoolInfo.dbn
    }
}
