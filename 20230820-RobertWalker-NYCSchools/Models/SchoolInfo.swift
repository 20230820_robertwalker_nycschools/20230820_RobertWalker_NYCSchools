//
//  SchoolInfo.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import Foundation

struct SchoolInfo: Decodable {
    let dbn: String
    let name: String
    let overview: String
    let neighborhood: String
    let streetAddress: String
    let city: String
    let state: String
    let zip: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case overview = "overview_paragraph"
        case neighborhood
        case streetAddress = "primary_address_line_1"
        case city
        case state = "state_code"
        case zip
    }
}
