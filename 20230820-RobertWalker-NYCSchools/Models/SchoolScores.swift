//
//  SchoolScores.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import Foundation

struct SchoolScores: Decodable {
    let dbn: String
    let testTakerCount: String
    let readingAverageScore: String
    let mathAverageScore: String
    let writingAverageScore: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case testTakerCount = "num_of_sat_test_takers"
        case readingAverageScore = "sat_critical_reading_avg_score"
        case mathAverageScore = "sat_math_avg_score"
        case writingAverageScore = "sat_writing_avg_score"
    }
}
