//
//  _0230820_RobertWalker_NYCSchoolsApp.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

@main
struct _0230820_RobertWalker_NYCSchoolsApp: App {

    let viewModel = SchoolListViewModel()

    var body: some Scene {
        WindowGroup {
            SchoolListContentView(viewModel: viewModel)
                .onAppear {
                    viewModel.refresh()
                }
        }
    }
}
