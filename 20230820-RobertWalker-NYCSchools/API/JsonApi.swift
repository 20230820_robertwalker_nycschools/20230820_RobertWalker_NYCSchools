//
//  JsonApi.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import Foundation

enum ApiError: Error {
    case malformedUrl
    case decoding
    case network
}

class JsonApi {
    let dataFetcher: DataFetcherProtocol

    init(fetcher: DataFetcherProtocol = URLSession.shared) {
        self.dataFetcher = fetcher
    }

    func performRequest<T: Decodable>(urlString: String) async throws -> T {
        guard let url = URL(string: urlString) else {
            throw ApiError.malformedUrl
        }

        do {
            let (data, _) = try await dataFetcher.getData(from: url)
            return try JSONDecoder().decode(T.self, from: data)
        } catch is DecodingError {
            throw ApiError.decoding
        } catch {
            throw ApiError.network
        }
    }
}

protocol DataFetcherProtocol {
    func getData(from: URL) async throws -> (Data, URLResponse)
}

extension URLSession: DataFetcherProtocol {
    func getData(from url: URL) async throws -> (Data, URLResponse) {
        return try await data(from: url)
    }
}
