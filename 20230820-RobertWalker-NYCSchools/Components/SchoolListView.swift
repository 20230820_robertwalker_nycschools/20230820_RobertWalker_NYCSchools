//
//  SchoolListView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct SchoolListView: View {
    private let viewModels: [SchoolViewModel]
    private let refreshHandler: () -> Void
    @State private var searchText = ""

    var searchResults: [SchoolViewModel] {
        if searchText.isEmpty {
            return viewModels
        } else {
            return viewModels.filter { $0.schoolInfo.name.lowercased().starts(with: searchText.lowercased()) }
        }
    }

    init(viewModels: [SchoolViewModel], refreshHandler: @escaping () -> Void = {}) {
        self.viewModels = viewModels
        self.refreshHandler = refreshHandler
    }

    var body: some View {
        NavigationView {
            List(searchResults) { viewModel in
                NavigationLink {
                    SchoolDetailView(viewModel: viewModel)
                } label: {
                    SchoolView(viewModel: viewModel)

                }.listRowSeparator(.hidden)
            }
            .listStyle(.plain)
            .navigationTitle("NYC Schools")
            .refreshable {
                refreshHandler()
            }
            .searchable(text: $searchText)
        }
    }
}
