//
//  SchoolListContentView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI
import Combine

struct SchoolListContentView: View {
    @ObservedObject var viewModel: SchoolListViewModel

    init(viewModel: SchoolListViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        switch viewModel.state {
        case .loading:
            LoadingView()
        case .error:
            ErrorView {
                viewModel.refresh()
            }
        case .loaded(let schoolViewModels):
            SchoolListView(viewModels: schoolViewModels) {
                viewModel.refresh()
            }
        }
    }
}

struct SchoolListContentView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListContentView(viewModel: SchoolListViewModel())
    }
}
