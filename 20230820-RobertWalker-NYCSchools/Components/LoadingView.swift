//
//  LoadingView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        VStack {
            ProgressView()
                .padding()
            Text("Loading Schools...")
        }
    }
}
