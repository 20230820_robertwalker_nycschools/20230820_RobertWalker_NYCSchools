//
//  ErrorView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct ErrorView: View {
    let refreshAction: () -> Void

    init(action: @escaping () -> Void) {
        self.refreshAction = action
    }

    var body: some View {
        VStack {
            Text("Oops, something went wrong. Tap refresh to try again.")
                .multilineTextAlignment(.center)
            Button("Refresh") {
                self.refreshAction()
            }
            .padding()
        }
    }
}
