//
//  SchoolCellView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct SchoolView: View {
    private let viewModel: SchoolViewModel
    private var schoolInfo: SchoolInfo {
        return viewModel.schoolInfo
    }

    init(viewModel: SchoolViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        VStack(alignment: .leading) {
            Text(schoolInfo.name)
                .font(.headline)

            HStack {
                if viewModel.hasScores {
                    PillTextView("SAT Scores", color: .green)
                }

                PillTextView(schoolInfo.neighborhood)
            }
        }
    }
}

struct PillTextView: View {
    let text: String
    let color: Color

    init(_ text: String, color: Color = .gray) {
        self.text = text
        self.color = color
    }

    var body: some View {
        HStack {
            Text(text)
                .font(.system(size: 14))
                .foregroundColor(.white)
                .padding(4)
        }
        .background(color)
        .cornerRadius(8)
    }
}
