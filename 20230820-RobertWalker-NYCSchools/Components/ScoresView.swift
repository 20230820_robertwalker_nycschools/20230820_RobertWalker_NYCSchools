//
//  ScoresView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct ScoresView: View {
    private let scores: SchoolScores

    init(scores: SchoolScores) {
        self.scores = scores
    }

    var body: some View {
        VStack(alignment: .leading) {
            Text("SAT Scores")
                .font(.headline.bold())
            LineItemView(leftText: "SAT Tests Taken:", rightText: scores.testTakerCount)
            LineItemView(leftText: "Avg Reading Score:", rightText: scores.readingAverageScore)
            LineItemView(leftText: "Avg Math Score:", rightText: scores.mathAverageScore)
            LineItemView(leftText: "Avg Writing Score:", rightText: scores.writingAverageScore)

        }
        .padding(8)
    }
}
