//
//  LineItemView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct LineItemView: View {
    private let leftText: String
    private let rightText: String

    init(leftText: String, rightText: String) {
        self.leftText = leftText
        self.rightText = rightText
    }

    var body: some View {
        HStack {
            Text(leftText)
            Spacer()
            Text(rightText)
        }
    }
}
