//
//  DescriptionView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct DescriptionView: View {
    private let description: String

    init(description: String) {
        self.description = description
    }

    var body: some View {
        VStack(alignment: .leading) {
            Text("Description")
                .font(.headline.bold())

            Text(description)
                .font(.subheadline)
        }
        .padding(8)
    }
}
