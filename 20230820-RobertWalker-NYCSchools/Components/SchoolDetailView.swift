//
//  SchoolDetailView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct SchoolDetailView: View {
    private let viewModel: SchoolViewModel
    private var schoolInfo: SchoolInfo {
        return viewModel.schoolInfo
    }
    private var schoolScores: SchoolScores? {
        return viewModel.schoolScores
    }

    init(viewModel: SchoolViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading) {
                DescriptionView(description: schoolInfo.overview)
                AddressView(info: schoolInfo)
                if let schoolScores = schoolScores {
                    ScoresView(scores: schoolScores)
                }

                Spacer()
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle(schoolInfo.name)
    }
}
