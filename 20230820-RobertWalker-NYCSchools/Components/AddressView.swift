//
//  AddressView.swift
//  20230820-RobertWalker-NYCSchools
//
//  Created by Robert Walker on 8/20/23.
//

import SwiftUI

struct AddressView: View {
    let schoolInfo: SchoolInfo

    init(info: SchoolInfo) {
        self.schoolInfo = info
    }

    var body: some View {
        VStack(alignment: .leading) {
            Text("Address")
                .font(.headline.bold())
            Text(schoolInfo.streetAddress)
            Text("\(schoolInfo.city), \(schoolInfo.state) \(schoolInfo.zip)")
        }
        .padding(8)
    }
}
