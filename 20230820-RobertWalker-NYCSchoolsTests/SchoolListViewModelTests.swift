//
//  SchoolListViewModelTests.swift
//  20230820-RobertWalker-NYCSchoolsTests
//
//  Created by Robert Walker on 8/20/23.
//

import XCTest
import Combine
@testable import _0230820_RobertWalker_NYCSchools

class MockJsonApi: JsonApi {
    var shouldThrow = false
    private let schools = [
        SchoolInfo(dbn: "a", name: "School A", overview: "", neighborhood: "", streetAddress: "", city: "", state: "", zip: ""),
        SchoolInfo(dbn: "b", name: "School B", overview: "", neighborhood: "", streetAddress: "", city: "", state: "", zip: ""),
        SchoolInfo(dbn: "c", name: "School C", overview: "", neighborhood: "", streetAddress: "", city: "", state: "", zip: "")
    ]

    private let scores = [
        SchoolScores(dbn: "a", testTakerCount: "1", readingAverageScore: "100", mathAverageScore: "100", writingAverageScore: "100"),
        SchoolScores(dbn: "b", testTakerCount: "2", readingAverageScore: "200", mathAverageScore: "200", writingAverageScore: "200"),
        SchoolScores(dbn: "c", testTakerCount: "3", readingAverageScore: "300", mathAverageScore: "300", writingAverageScore: "300"),
    ]

    override func performRequest<T: Decodable>(urlString: String) async throws -> T {
        if shouldThrow {
            throw NSError()
        }

        if T.self == [SchoolInfo].self {
            return schools as! T
        } else {
            return scores as! T
        }
    }
}

final class SchoolListViewModelTests: XCTestCase {

    var cancellables = Set<AnyCancellable>()

    func testViewModelLoadedState() async throws {
        let mockJsonApi = MockJsonApi()
        let expectation = expectation(description: "state is .loaded")

        let listViewModel = SchoolListViewModel(api: mockJsonApi)
        await listViewModel.refresh()
        let sub = listViewModel.$state.receive(on: RunLoop.main).sink { state in
            if case SchoolListViewModel.ViewModelState.loaded(let models) = state {
                XCTAssert(models.count == 3)

                expectation.fulfill()
            }
        }
        cancellables.insert(sub)
        await fulfillment(of: [expectation], timeout: 1)

    }

    func testViewModelErrorState() async throws {
        let mockJsonApi = MockJsonApi()
        mockJsonApi.shouldThrow = true
        let expectation = expectation(description: "state is .error")
        let listViewModel = SchoolListViewModel(api: mockJsonApi)
        await listViewModel.refresh()

        let sub = listViewModel.$state.sink { state in
            if case SchoolListViewModel.ViewModelState.error = state {
                expectation.fulfill()
            }
        }
        cancellables.insert(sub)
        await fulfillment(of: [expectation], timeout: 1)
    }
}
