//
//  JsonApiTests.swift
//  20230820-RobertWalker-NYCSchoolsTests
//
//  Created by Robert Walker on 8/20/23.
//

import XCTest
@testable import _0230820_RobertWalker_NYCSchools

class MockDataFetcher: DataFetcherProtocol {

    var responseData = Data()
    var shouldThrow = false

    func getData(from: URL) async throws -> (Data, URLResponse) {
        if shouldThrow {
            throw NSError()
        }
        return (responseData, URLResponse())
    }
}

struct TestModel: Decodable {
    let property1: String
    let property2: String
}

let modelJsonString = "{\"property1\":\"one\",\"property2\":\"two\"}"
let badJsonString = "{}"

final class JsonApiTests: XCTestCase {

    var mockFetcher: MockDataFetcher!
    var jsonApi: JsonApi!

    override func setUpWithError() throws {
        mockFetcher = MockDataFetcher()
        mockFetcher.responseData = modelJsonString.data(using: .utf8)!
        jsonApi = JsonApi(fetcher: mockFetcher)
    }

    override func tearDownWithError() throws {

    }

    func testFetchAndParseData() async throws {
        do {
            let testModel: TestModel = try await jsonApi.performRequest(urlString: "http://www.test.com")
            XCTAssert(testModel.property1 == "one")
            XCTAssert(testModel.property2 == "two")

        } catch {
            XCTFail("should not throw")
        }
    }

    func testBadUrl() async throws {
        do {
            let _: TestModel = try await jsonApi.performRequest(urlString: "   *bad.url")
            XCTFail("bad url should throw")
        } catch {
            XCTAssert((error as? ApiError) == .malformedUrl)
        }
    }

    func testBadParse() async throws {
        do {
            mockFetcher.responseData = badJsonString.data(using: .utf8)!
            let _: TestModel = try await jsonApi.performRequest(urlString: "http://test.com")
            XCTFail("bad response should throw")
        } catch {
            XCTAssert((error as? ApiError) == .decoding)
        }
    }

    func testFetcherThrow() async throws {
        do {
            mockFetcher.shouldThrow = true
            let _: TestModel = try await jsonApi.performRequest(urlString: "http://test.com")
            XCTFail("bad network should throw")
        } catch {
            XCTAssert((error as? ApiError) == .network)
        }
    }
}
